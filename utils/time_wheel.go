package utils

import (
	"container/list"
	"fmt"
	"sync"
	"time"
)

type taskElement struct {
	cycle int    //时间轮的轮数
	pos   int    //定时任务在环形数组中的所在位置
	key   string //全局唯一的key
	task  func() //定时任务要执行的方法
}

// TimeWheel 时间轮算法的实现
type TimeWheel struct {
	sync.Once                          //保证时间轮结束的时候唯一
	interval  time.Duration            //时间轮的运行时间间隔
	ticker    *time.Ticker             //时间轮定时器
	stopic    chan interface{}         //停止时间轮的channel
	addTaskCh chan *taskElement        //新增定时任务的channel
	delTaskCh chan string              //删除定时任务的channel
	slots     []*list.List             //时间轮的槽位存放具体的任务
	curSlot   int                      //遍历到的当前环形数组的下标
	keyToTask map[string]*list.Element //集中管理的key和定时任务,便于进行删除
}

// 添加定时任务
func (t *TimeWheel) addTask(task *taskElement) {
	//找到对应的时间轮的位置
	list1 := t.slots[task.pos]
	//查看key是否存在，如果存在的话，就删除此任务并且创建一个新的任务
	if _, ok := t.keyToTask[task.key]; ok {
		//删除任务
		t.delTask(task.key)
	}
	//添加定时任务到链表的尾部
	eTask := list1.PushBack(task)
	//添加key到集合中统一管理起来
	t.keyToTask[task.key] = eTask
}

// 删除定时任务
func (t *TimeWheel) delTask(key string) {
	//找对应的定时任务
	eTask, err := t.keyToTask[key]
	if !err {
		fmt.Println("定时任务不存在")
		return
	}
	delete(t.keyToTask, key)
	//从list链表中删除任务
	task, _ := eTask.Value.(*taskElement)
	t.slots[task.pos].Remove(eTask)
}

// 计算定时任务在环形数组的下标和时间
func (t *TimeWheel) getPosAndCycle(executeAt time.Time) (int, int) {
	delay := int(time.Until(executeAt))                       //计算刚才到现在的时间
	cycle := delay / (len(t.slots) * int(t.interval))         //定时任务需要的轮的数量
	pos := (t.curSlot + delay/int(t.interval)) % len(t.slots) //求出下标位置
	return pos, cycle
}

// 指针向前移动
func (t *TimeWheel) circuleAdd() {
	t.curSlot = (t.curSlot + 1) % len(t.slots)
}

// 批量处理的定时任务
func (t *TimeWheel) execute(list *list.List) {
	//遍历每个list
	for e := list.Front(); e != nil; {
		taskElement, _ := e.Value.(*taskElement)
		if taskElement.cycle > 0 {
			taskElement.cycle--
			e = e.Next()
			continue
		}
		//执行任务的情况
		go func() {
			defer func() {
				if err := recover(); err != nil {
					fmt.Println("执行任务失败就重新再试")
					return
				}
			}()
			taskElement.task() //执行任务
		}()
		next := e.Next()
		list.Remove(e)
		delete(t.keyToTask, taskElement.key)
		e = next
	}
}

// 时间轮定时器
func (t *TimeWheel) tick() {
	list1 := t.slots[t.curSlot] //取出对应的链表并进行向前推移
	defer t.circuleAdd()        //指针向前推移
	//批量处理满足条件的定时任务
	t.execute(list1)
}

// 启动时间轮
func (t *TimeWheel) run() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("时间轮启动失败")
			panic(err)
		}
	}()
	for {
		select {
		case <-t.stopic:
			//停止时间轮
			return
		case <-t.ticker.C:
			//时间轮定时器
			t.tick()
		case task := <-t.addTaskCh:
			//添加定时任务
			t.addTask(task)
		case delString := <-t.delTaskCh:
			//删除定时任务
			t.delTask(delString)
		}
	}
}

// Stop 停止时间lun
func (t *TimeWheel) Stop() {
	t.Do(func() {
		t.ticker.Stop()
		close(t.stopic)
	})
}

// AddTask 对外添加定时任务的接口(传入唯一的key，要执行的函数，还有一个执行的时间）
func (t *TimeWheel) AddTask(key string, task func(), executeAt time.Time) {
	//计算在环形数组的下标，和时间
	pos, cycle := t.getPosAndCycle(executeAt)
	t.addTaskCh <- &taskElement{
		cycle: cycle,
		pos:   pos,
		key:   key,
		task:  task,
	}
}

// DelTask 对外删除定时任务的接口
func (t *TimeWheel) DelTask(key string) {
	t.delTaskCh <- key
}

// NewTimeWheel 初始化时间轮
func NewTimeWheel(slotNum int, interval time.Duration) *TimeWheel {
	//当注册的时间轮槽位小于0的时候就定义为10
	switch {
	case slotNum <= 0:
		slotNum = 10
	case slotNum >= 500:
		slotNum = 500
	}
	//当注册时间间隔小于0的时候自动改为1秒
	if interval <= 0 {
		interval = time.Second
	}
	//实例化时间论
	t := TimeWheel{
		interval:  interval,
		ticker:    time.NewTicker(interval),
		stopic:    make(chan interface{}),
		addTaskCh: make(chan *taskElement),
		delTaskCh: make(chan string),
		slots:     make([]*list.List, 0, slotNum),
		keyToTask: make(map[string]*list.Element),
	}
	for i := 0; i < slotNum; i++ {
		t.slots = append(t.slots, list.New()) //实例化具体的任务列表
	}
	go t.run()
	return &t
}

//func test() {
//	timeWheel := NewTimeWheel(10, 500*time.Millisecond)
//	defer timeWheel.Stop()
//	timeWheel.AddTask("test1", func() {
//		fmt.Println("test1", time.Now())
//	}, time.Now().Add(1*time.Second))
//	timeWheel.delTask("test1")
//	timeWheel.AddTask("test2", func() {
//		fmt.Println("test2", time.Now())
//	}, time.Now().Add(2*time.Second))
//	<-time.After(10 * time.Second)
//}
